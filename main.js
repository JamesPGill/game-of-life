liveCellsData = [[1,3],[3,3],[5,3],[0,4],[2,4],[4,4],[6,4]]


function createEmptyBoard(width, height) {
    board = []
    for (let j =0; j < height; j++) {
        row =[]
        for (let i =0; i < width; i++){
            row.push(0)
        }
        board.push(row)
    }
    return board
}

function initialiseBoard(livecells, board) {
    for (let i =0; i < livecells.length; i++) {
        let livecell = livecells[i]
        board[livecell[0]][livecell[1]] = 1
    }
}

function countNeighbour(cell, board){
  count = 0
  if (cell[1] == 0){
    if (cell[0] == 0){
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
    }
    else if (cell[0] == board.length-1) {
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
    }
    else {
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]+1]
    }
  }

  else if (cell[1] == board[0].length-1){
    if (cell[0] == 0){
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]-1]
      count += board[cell[0]][cell[1]-1]
    }
    else if (cell[0] == board.length-1) {
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]-1]
      count += board[cell[0]][cell[1]-1]
    }
    else {
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]-1]
      count += board[cell[0]][cell[1]-1]
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]-1]
    }
  }

  else if (cell[0] == 0){
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]-1]
      count += board[cell[0]+1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
      count += board[cell[0]][cell[1]-1]
  }

  else if (cell[0] == board.length-1){
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]-1]
      count += board[cell[0]-1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
      count += board[cell[0]][cell[1]-1]
  }

  else {
    count += board[cell[0]-1][cell[1]]
    count += board[cell[0]-1][cell[1]-1]
    count += board[cell[0]-1][cell[1]+1]
    count += board[cell[0]][cell[1]+1]
    count += board[cell[0]][cell[1]-1]
    count += board[cell[0]+1][cell[1]]
    count += board[cell[0]+1][cell[1]-1]
    count += board[cell[0]+1][cell[1]+1]
  }
  return count
}

function neighbourCountBoard(board){
  countBoard = []
  for (let i=0; i < board.length; i++){
    countRow = []
    for (let j=0; j <board[0].length; j++){
      countRow.push(countNeighbour([i,j], board))
    }
    countBoard.push(countRow)
  }
  return countBoard
}

function isLivingInASecond(cell, countBoard, board){
  let alive
  if (board[cell[0]][cell[1]] == 1) {
    if (0 <= countBoard[cell[0]][cell[1]] < 2){
      alive = 0
    }
    else if (4 <= countBoard[cell[0]][cell[1]]){
      alive = 0
    }
    else {
      alive = 1
    }
  }
  else if (board[cell[0]][cell[1]] == 0) {
    if (0 <= countBoard[cell[0]][cell[1]] && countBoard[cell[0]][cell[1]] <= 2){
      alive = 0
    }
    else if (4 <= countBoard[cell[0]][cell[1]] && countBoard[cell[0]][cell[1]] <= 8){
      alive = 0
    }
    else if (countBoard[cell[0]][cell[1]]==3){

      alive = 1
    }
  }
  return alive
}

function refreshBoard(board) {
  countBoard = neighbourCountBoard(board)
  for (let i=0; i < board.length; i++){
    newRow = []
    for (let j=0; j <board[0].length; j++){
      board[i][j] = isLivingInASecond([i,j], countBoard, board)
    }
  }
}

function playGame(){
  
}
